<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/aboute',function(){
    return view('aboute');
});


Route::get('/profile', function () {
    return view('profile');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/home', function () {
    return view('cms/homepage');
});